<?php
class Database {

	/*
	-	Creating the class variables
	-	and some constants
	*/
		private static $DBHOST = "127.0.0.1";
		private static $DBDATA = "test";
		private static $DBUSER = "root";
		private static $DBPASS = "";
		private static $_conx = null;

		private $_pdo,
				$_query,
				$_results,
				$_count = 0,
				$_error = false;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/

		private function __construct() {
			try {
				$this->_pdo = new PDO("mysql:host=" . self::$DBHOST . ";dbname=" . self::$DBDATA, self::$DBUSER, self::$DBPASS);
			} catch (PDOException $e) {
				die($e->getMessage());
			}
		}

	/*
	-	this method will connect to
	-	the database only if the connection
	-	is not already set. This way we avoid multiple
	-	database Connections.
	-	
	-	If the connection is set then we
	-	simply return the connection to the requesting class.
	*/

		public static function dbinit() {
			if (!isset(self::$_conx)) {
				self::$_conx = new Database();
			}
			return self::$_conx;
		}

	/*
	-	This is the query method, it will query the databse
	-	and return objects. It will bind the values query(ed)
	- 	to avaid any sql Injection or database holes.
	-	In this way only the system admins can end the sql query.
	*/

		public function query($SQL_STRING, $PARMS = array()) {
			
			// first we set the errors to non (false)
			$this->_error = false;

			// we will prepare the sql query string
			if ($this->_query = $this->_pdo->prepare($SQL_STRING)) {

				// count the PARMS ans assign a ? mark to replace
				if (count($PARMS)) {
					$i = 1;
					foreach ($PARMS as $PARM) {
						$this->_query->bindValue($i, $PARM);
						$i++;
					}
				}

				// continueing to execution. If the execute returns true
				// we continue or we will set an error (error = true)
				if ($this->_query->execute()) {
					// ASSIGNING RESULTS AND NUM_ROWS
					$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
					$this->_count = $this->_query->rowCount();
				} else {
					$this->_error = true;
				}
			}

			// returning this obj
			return $this;
		}


	/*
	-	This is the insert method. It will build the query string
	-	for insertion. It takes the table and an array of fields => input
	-	but we will build it så it will asgin the field => ? in query
	-	So no one can say "; DROP TABLE users" - For safety
	*/

		public function insert($TABLE, $FIELDS = array()) {

			// getting the array keys (fieldname=>input) 
			$fieldnames = array_keys($FIELDS);
			$value = null;
			$i = 1;

			// looping trough the array of fields and 
			// assigning ? makr for each input-value
			foreach ($FIELDS as $f) {
				$value .= "?";
				if ($i < count($FIELDS)) {
					$value .= ", ";
				}
				$i++;
			}

			// SQL_STRING will be like:
			$SQL_STRING = "INSERT INTO {$TABLE} (`". implode('`, `', $fieldnames) ."`) VALUES ({$value})";

			// Call to query (exe)
			if (!$this->query($SQL_STRING, $FIELDS)->error()) {
				// success
				return true;
			}

			// error / failed / connection problems
			return false;
		}


	/*
	-	This is the update method that will handle any updates that is needed at anytime.
	-	it will require a table, fields, the identifier of a row and the fieldname of the identifier.
	-	defaults: ID_FIELDNAME will be ID
	*/

		public function update($TABLE, $FIELDS, $IDENTIFIER, $ID_FIELDNAME = "ID") {
			
			// Making the variables ready for 
			// use. SQL_STRING as usual, i for loop
			$SQL_STRING_SET = ""; 
			$i = 1;

			// Again we will assign fieldnames to ? mark this time the as a SET
			foreach ($FIELDS as $name => $value) {
				$SQL_STRING_SET .= "{$name} = ?";
				if($i < count($FIELDS)) {
					$SQL_STRING_SET .= ", ";
				}
				$i++;
			}

			// build the string
			$SQL_STRING = "UPDATE {$TABLE} SET {$SQL_STRING_SET} WHERE {$ID_FIELDNAME} = {$IDENTIFIER}";

			// call to exe
			if(!$this->query($SQL_STRING, $FIELDS)->error()) {
				// success
				return true;
			}

			// failed, unsuccessfull
			return false;
		}


	/*
	-	This is the update method that will handle any selects that is needed at anytime.
	-	it will require a table, fields, the identifier of a row and the fieldname of the identifier.
	-	defaults: NONE
	*/


		public function select($TABLE, $FIELDS, $GET) {
			$SQL_STRING_SET = "";
			$i = 1;

			foreach ($FIELDS as $name => $value) {
				$SQL_STRING_SET .= "{$name} = ?";
				if($i < count($FIELDS)) {
					$SQL_STRING_SET .= " AND ";
				}
				$i++;
			}

			$SQL_STRING = "SELECT {$GET} FROM {$TABLE} WHERE {$SQL_STRING_SET}";

			if(!$this->query($SQL_STRING, $FIELDS)->error()) {
				return $this;
			}

			return false;
		}




	/*
	-	This is the action method that will handle operators
	-	such as deleteing something etc
	-	it will take an action, table and array of action-values
	*/

		public function action($ACTION, $TABLE, $WHERE = array()) {
			// checking if the WHERE clause has all parameters
			if(count($WHERE) === 3) {
				// creating allowed operators
				$allowed_operators = array("=", "<", ">", "<=", "=>");

				// getting the field operator, value.
				$field 		= $WHERE[0];
				$operator 	= $WHERE[1];
				$value 		= $WHERE[2];

				// checking if the operator is allowed to be used within our query
				if(in_array($operator, $allowed_operators)) {
					$SQL_STRING = "{$ACTION} FROM {$TABLE} WHERE {$field} {$operator} ?";
					if(!$this->query($SQL_STRING, array($value))->error()) {
						// return the obj
						return $this;
					}
				}
			}

			// fail
			return false;
		}

	/*
	-	Call to action methos
	-	the get and delete will handle getting info
	-	or deleting info from the databse.
	*/

		public function get($TABLE, $WHERE) {
			return $this->action("SELECT *", $TABLE, $WHERE);
		}

		public function del($TABLE, $WHERE) {
			return $this->action("DELETE", $TABLE, $WHERE);
		}


	/*
	-	this is the class returners, they will handle any required 
	-	value that is needed and return it to the requesting class
	*/

		public function results() {
			return $this->_results;
		}

		public function result() {
			return $this->_results[0];
		}

		public function lastInsertId() {
			return $this->_pdo->lastInsertId();
		}
	
		public function count() {
			return $this->_count;
		}

		public function error() {
			return $this->_error;
		}
}