<?php
class Encrypt {
	public static function make($string, $salt = '') {
		return hash('sha256', $string . $salt);
	}

	public static function salt() {
		return substr(self::make(microtime(), time()), 16, 32);
	}

	public static function unique() {
		return self::make(uniqid());
	}
}