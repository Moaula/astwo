<?php
class Get {

	/*
	-	Creating class variables
	*/
		private $_db,
				$_data,
				$_results,
				$_result,
				$_count;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/

		public function __construct() {
			$this->_db = Database::dbinit();
		}

	/*
	-	Select all from spesific table
	-	limit the selection to a value
	-	use desc or asc
	*/

		public function get_data($table, $identifier = "ID", $order = "ASC", $limit = 1000) {
			$query = $this->_db->query("SELECT * FROM {$table} ORDER BY {$identifier} {$order} LIMIT {$limit}");
			if($query->count()) {
				$this->_results = $query->results();
				$this->_result  = $query->result();
				return true;
			}
			return false;
		}

		public function get_data_by_id($table, $identifier = "ID", $id, $order = "ASC", $limit = 1000) {
			$query = "SELECT * FROM {$table} WHERE {$identifier} = ? ORDER BY {$identifier} {$order} LIMIT {$limit}";
			$value = array($id);
			$data = $this->_db->query($query, $value);
			if($data->count()) {
				$this->_results = $data->results();
				$this->_result = $data->result();
			}
		}

	/*
	-	check if a user have voted
	*/

		public function vote_check($TABLE, $FIELDS = array(), $CHECK = "ID") {
			$query = $this->_db->select($TABLE, $FIELDS, $CHECK);
			if($query->count()) {
				$this->_count = $query->count();
				return true;
			}
			return false;
		}

	/*
	-	Return the result or false
	- 	if the methods have results
	*/

		public function results() {
			return $this->_results;
		}

		public function result() {
			return $this->_result;
		}

		public function count() {
			return $this->_count;
		}

}	
