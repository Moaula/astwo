<?php
class Token {
	private static $SESSION_TOKEN = "this_user_token";

	public static function create() {
		return Session::create(self::$SESSION_TOKEN, hash('sha256', uniqid().microtime())); 
		//return Session::create(self::$SESSION_TOKEN, mcrypt_create_iv(32, MCRYPT_DEV_URANDOM)); 
		
	}

	public static function check($token) {
		if (Session::exists(self::$SESSION_TOKEN) && Session::get(self::$SESSION_TOKEN) === $token) {
			Session::delete(self::$SESSION_TOKEN);
			return true;
		}
		return false;
	}
}