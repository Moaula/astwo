<?php 
class User {

	/*
	-	Creating the class variables
	*/
		private $_db,
				$_data,
				$_sessionName,
				$_coookieName,
				$_coookieTime,
				$_isOnline,
				$_results;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/
		public function __construct($uzr = null) {
			$this->_db = Database::dbinit();
			$this->_sessionName = "this_user";
			$this->_coookieName = "this_user_cookie";
			$this->_coookieTime = 64000;

			if (!$uzr) {
				if(Session::exists($this->_sessionName)) {
					$uzr = Session::get($this->_sessionName);
					if($this->find($uzr)) { $this->_isOnline = true; } else { $this->logout(); }
				}
			} else {
				$this->find($uzr);
			}
		}

	/*
	-	Now we will make the find method that will find
	-	a user by either the email or user id assignet to that user.
	*/
		public function find($uzr = null) {
			if($uzr) {
				$fieldname = (is_numeric($uzr)) ? 'UserID' : 'Username';
				$data = $this->_db->get('users', array($fieldname, '=', $uzr));
				if($data->count()) { $this->_data = $data->result(); return true; }
			}
			return false;
		}

	/*
	-	This method will create a user, and insert its data to 
	-	the database.
	*/
		public function create($fields = array()) {
			if(!$this->_db->insert('users', $fields)) {
				throw new Exception("En feil har oppstått. Feilkode: #56-UZRDB_USER");
			}
		}

	/*
	-	This method will update a user, and 
	-	its data in the database
	*/
		public function update($fields = array(), $UserID = null) {
			if (!$UserID && $this->isOnline()) { $UserID = $this->data()->UserID; }
			if (!$this->_db->update('users', $fields, $UserID, "UserID")) {
				throw new Exception("En feil har oppstått. Feilkode: #67-UZRDB_USER");
			}
		}

	/*
	-	This method will log the user into
	-	the system and grant it acces with associated
	-	user group privliges
	*/
		public function login($EmailAddress = null, $Password = null, $Remember = false) {
			if (!$EmailAddress && !$Password && $this->exists()) {
				Session::create($this->_sessionName, $this->data()->UserID);
			} else {
				$this_user = $this->find($EmailAddress);
				if($this_user) {
					if($this->data()->Password === Encrypt::make($Password, $this->data()->SLS)) {
						Session::create($this->_sessionName, $this->data()->UserID);

						/*
						- 	if the user chooses to let the system
						-	remember them to be logged in, we will save a random hash
						-	linked to their user id so we can auto log in next time 
						-	they visits the site.
						*/

						if($Remember) {
							$Encryption = Encrypt::unique();
							$Check_encryption = $this->_db->get('sessionscookies', array('UserID', '=', $this->data()->UserID));
							if(!$Check_encryption->count()) {
								$this->_db->insert('sessionscookies', array(
									'UserID'		=> $this->data()->UserID,
									'SessionName'	=> $Encryption
								));
							} else {
								$Encryption = $Check_encryption->result()->SessionName;
							}

							Cookie::create($this->_coookieName, $Encryption, $this->_coookieTime);
						}

						return true;
					}
				}
			}

			return false;
		}

	/*
	-	The methods below are just small checks
	-	and data returners.
	*/
		public function data() {
			return $this->_data;		
		}

		public function results() {
			return $this->_results;
		}

		public function exists() {
			return (!empty($this->_data)) ? true : false;
		}

		public function lastid() {
			return $this->_db->lastInsertId();
		}

		public function isOnline() {
			return $this->_isOnline;
		}

	/*
	-	This method will handle user level filtering
	-	it will only allow users with a spesefic group id
	-	to enter som parts of the website.
	*/
		public function hasPerm($perm) {
			$uzrgrp = $this->_db->get('usergroups', array('UserGroupID', '=', $this->data()->Usergroup));
			if ($uzrgrp->count()) {
				$permissions = json_decode($uzrgrp->result()->Permissions, true);
				if($permissions[$perm] == true) { return true; }
			}
			return false;
		}

	/*
	-	This method will activate a user account
	-	when a user verifies their email address.
	*/
		public function activate($email, $code) {
			$user_account = $this->find($email);
			if($user_account) {
				if($this->data()->Active == $code) {
					if(!$this->_db->update('users', array("Active" => "1"), $this->data()->UserID, "UserID")) {
						throw new Exception("En feil har oppstått. Feilkode: #161-UZRDB_USER");	
					}
				} 

				if($this->data()->Active == 1) {
					throw new Exception("Kontoen er allerede aktivert.");
				}

				if($this->data()->Active == "Banned") {
					throw new Exception("Kontoen er sperret i ubegrenset tid.");
				}
			}
		}

	/*
	-	This method will recover a username or password
	-	when the user cannot remember.
	*/
		public function recovery($fn = null, $ln = null, $bd = null, $em = null) {

			if(!$fn && !$ln && !$bd && !empty($em)) {

			} else {
				$email = strtolower(trim($em));
				$firstname = strtolower(trim($fn));
				$lastname = strtolower(trim($ln));
				$birthday = $bd;

				$qry = $this->_db->select('users', array(
					'FirstName' => $firstname,
					'LastName'	=> $lastname,
					'BirthDay'	=> $birthday
				), 'EmailAddress');

				if($qry->count()) {
					$data = $qry->result();
					recover_username($email, $data->EmailAddress, $firstname, $lastname);
					Session::flash('login', 'Vi har nå sendt deg ditt brukernavn på e-posten du har angitt');
					Redirect::to('/login');
				}
			}

			Session::flash('login', 'En feil har oppstått, prøv igjen senere.');
			Redirect::to('/login');
		}


	/*
	-	This is a logout method 
	-	it simply logs a user out and deletes
	-	all cookies form the system linked to the user
	*/
		public function logout() {
			$this->_db->del('sessionscookies', array('UserID', '=', $this->data()->UserID));
			Session::delete($this->_sessionName);
			Cookie::delete($this->_coookieName);
		}
}