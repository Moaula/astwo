<?php
class Validate {

	/*
	-	Creating the class variables
	-	and some constants
	*/
		private $_success 	= false,
				$_errors	= array(),
				$_db		= null;

	/*
	-	Constructing the class
	-	in this process we connect to the database
	-	every time it constructs it reconnects to the database
	*/
		public function __construct() {
			$this->_db = Database::dbinit();
		}

	/*
	-	Validation method, it will be used to check every
	-	input that is added trough the site.
	*/

		public function validator($INPUT, $ITEMS = array()) {

			/*
			-	Looping trough the inputed values
			-	and checking each and every input for 
			-	required, max, min, unique in db. 
			*/

			foreach($ITEMS as $item => $rules) {
				foreach($rules as $rule => $rulevalue) {

					/*
					-	Removing tags and lowering the characters
					-	for the inputs before checking.
					*/

					$item = strtolower($item);
					$vall = trim(strip_tags($INPUT[$item]));

					if($rule === "name" && empty($vall)) { $name = $rulevalue; }
					if($rule === "must" && empty($vall)) { $this->adderror("Feltet {$name} kreves."); return $this; } else {

						/*
						-	Now we switch trough the rules added and for each 
						-	we do the necessary checks.
						*/

						switch ($rule) {
							case 'name': $fieldname = $rulevalue; break;
							case 'newp': $newpasswo = $rulevalue; break;
							case 'minv': if(strlen($vall) < $rulevalue) { $this->adderror(" {$fieldname} må minst være {$rulevalue} tegn! "); } break;
							case 'maxv': if(strlen($vall) > $rulevalue) { $this->adderror(" {$fieldname} kan maksimum være {$rulevalue} tegn! "); } break;
							case 'same': if($vall != $INPUT[$rulevalue]) { $this->adderror(" {$newpasswo} stemmer IKKE overens med {$fieldname} "); } break;
							case 'prim': $thevall = trim($INPUT[$item]);
										 if(!empty($thevall)) {
										 	$dblup = $this->_db->query("SELECT * FROM {$rulevalue} WHERE {$item} = '$vall'");
										 	if(!empty($dblup->results())) {
										 		$this->adderror(" {$fieldname} er allerede registert, vennligst velg noe annet.");
										 	}
										 } 
										 break;
							case 'exis': $thevall = trim($INPUT[$item]);
										 if(!empty($thevall)) {
										 	$dblup = $this->_db->query("SELECT * FROM {$rulevalue} WHERE {$item} = '$thevall'");
										 	if(!empty($dblup->results())) {
										 		$this->adderror(" {$fieldname} finnes ikke i våre systmer.");
										 	}
										 } 
										 break;
						}

					}

				}
			}

			/*
			-	If there is no errors then we 
			-	will set suckess to true. And return the OBJ
			*/

			if (empty($this->_errors)) {
				$this->_success = true;
			}

			return $this;
		}

	/*
	-	This method will add the errors to an errors array
	-	while we are checking inputed values. if any.
	*/
		private function adderror($err) {
			$this->_errors[] = $err;
		}

	/*
	-	Those two method will return either error or 
	-	a success message to the user.
	*/
		public function errors() {
			return $this->_errors;
		}

		public function success() {
			return $this->_success;
		}
}