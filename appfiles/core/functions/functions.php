<?php 
//	WriteAlerts function:
	function writealerts($string, $type = 'success', $class = 'default-class') {
		return '
		<div class="alert alert-dismissible alert-'. $type .' '. $class .'" role="alert"> 
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">x</span>
				<span class="sr-only">Close</span>
			</button>
			'. $string .'
		</div>';
	}

//	WriteErrors function:
	function writeerrors($errors, $class = 'default-class', $header = 'Vennligst sjekk:') {
		return '
		<div class="alert alert-dismissible alert-danger '. $class .'" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">x</span>
				<span class="sr-only">Close</span>
			</button>
			<h4>'. $header .'</h4>
			<ul><li>' .	implode('</li><li>', $errors) . '</li></ul>
		</div>';
	}

//	Thumbs maker
	function create_thumbs($source, $product, $ext, $w, $h) {
		$wanted_width = $w;
		$wanted_height = $h;

		list($orginal_width, $orginal_height) = getimagesize($source);
		$scale = ($orginal_width / $orginal_height);

		if (($wanted_width / $wanted_height) > $scale) {
			$wanted_width = $wanted_height * $scale;
		} else {
			$wanted_height = $wanted_width / $scale;
		}

		$image = '';
		if ($ext == 'gif') {
			$image = imagecreatefromgif($source);
		} else if ($ext == 'png') {
			$image = imagecreatefrompng($source);
		} else {
			$image = imagecreatefromjpeg($source);	
		}

		$blackbox = imagecreatetruecolor($wanted_width, $wanted_height);
		imagealphablending($blackbox, false);
		$col = imagecolorallocatealpha($blackbox, 240, 240, 240, 0);
		imagefilledrectangle($blackbox, 0, 0, $wanted_width, $wanted_height, $col);
		imagealphablending($blackbox, true);

		imagecopyresampled($blackbox, $image, 0, 0, 0, 0, $wanted_width, $wanted_height, $orginal_width, $orginal_height);
		
		if ($ext == 'gif') {
			imagegif($blackbox, $product, 100);
		} else if ($ext == 'png') {
			imagepng($blackbox, $product, 9);
		} else {
			imagejpeg($blackbox, $product, 90);
		}
	}
