<?php

/*
-	Startion the session and setting the error_reporting
-	error_reporting states: Production: 0 and Developement: E_ALL
*/
	session_start();
	error_reporting(E_ALL);
/*
-	Loading the class library and 
-	other php system functions.
*/
	
	spl_autoload_register(function($class) {
		$dirname = dirname(__FILE__);
		require_once $dirname . '/classes/' . $class . '.php';
	});

	$dirname = dirname(__FILE__);
	require_once $dirname . '/functions/functions.php';
	require_once $dirname . '/functions/mailing.php';
/*
-	get the current document.
-	used for activating buttons and giving page titles.
*/
	$CDoc = $_SERVER['PHP_SELF'];
	$CDoc = explode('/', $CDoc);
	$CDoc = $CDoc[count($CDoc) - 1];
	$CDoc = explode('.', $CDoc);
	$CDoc = $CDoc[count($CDoc) - 2];
/*
-	System "global" variables will
-	be set here:
*/
	$_init_uzer = new User();
	$_init_uzer_data = $_init_uzer->data();

/*
-	Auto login if user have a
-	saved cookie for keepme
-	logged inn
*/
	if (Cookie::exists("this_user_cookie") && !Session::exists("this_user")) {
		$this_user_cookie = Cookie::get("this_user_cookie");
		$this_encrypted_user = Database::dbinit()->get('sessionscookies', array('SessionName', '=', $this_user_cookie));

		if($this_encrypted_user->count()) {
			$UserID = $this_encrypted_user->result()->UserID;
			$User = new User($UserID);

			$user_data = Database::dbinit()->get('users', array('UserID', '=', $UserID));
			$user_data = $user_data->result();

			$User->login();
			Session::flash('index', 'Hey '. ucfirst($user_data->Firstname). ' ' . ucfirst($user_data->Lastname) . '. Velkommen tilbake :)');
			Redirect::to('/');
		}
	}