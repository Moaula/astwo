$(function(){

	$(".menuItem").click(function(){
		$(".menuItem").removeClass("red-circle");
		$(this).addClass("red-circle");
	}); 

	$(".initvote").click(function(){
		var vdo, vid, vrate;
		vdo = $(this).attr("data-do");
		vid = $(this).attr("data-id");
		vrate = $(this).attr("data-postfix");

		if(vdo == "give" || vdo == "take") {
			if($.isNumeric(vid)) {
				$.post("rate.php", { vdo: vdo, id: vid, votes: vrate }, function(data) {
					if(!$.isNumeric(data)) {
						$('.feedback').text(data);
						$('.feedback').css({ top: 60 }).show();
						$('.feedback').stop().animate({top: 60  }, 100).delay(3000).fadeOut();
					}
					if($.isNumeric(data)) {
						$(".votes_" + vid).text(data);
						$('.feedback').text("Takk for din stemme");
						$('.feedback').css({ top: 60 }).show();
						$('.feedback').stop().animate({top: 60  }, 100).delay(3000).fadeOut();	
					}
				});
			}
		}
	});
	
});