<?php 
	include_once('config/changepw-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container medium-form form-center">
			<?php 
				// feedback 
				if(Session::exists('changepw')) { 
					echo writealerts(Session::flash('changepw'), 'success', 'flat'); 
				} 

				// error feedback
				if(!empty($errors)) { 
					echo writeerrors($errors, 'flat', 'Feil'); 
				} 
			?>

			<div class="entry-title">Bytt passord</div>
			<div class="row">
				<form action="" method="post">
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Gjeldene passord</label>
							<input type="password" name="old_password" class="form-control" placeholder="Hva er ditt gjeldende passord ?" />
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Nytt passord (minst 8 tegn)</label>
							<input type="password" name="new_password" class="form-control" placeholder="Lag et nytt passord" />
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Bekreft passord</label>
							<input type="password" name="rep_password" class="form-control" placeholder="Bekreft ditt nye passord" />
						</div>
					</div>


					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<input type="hidden" name="update_user_password_token" value="<?php echo Token::create(); ?>">
							<button class="btn btn-primary"><i class="fa fa-refresh"></i> &nbsp; Oppdater </button>
							<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>