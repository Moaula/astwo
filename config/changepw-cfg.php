<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }


/*
-	Now we will go trough the posted 
-	input values and validate them
-	with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('update_user_password_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'old_password' => array(
					'name'	=> 'Gjeldene passord',	// if any errors display this name.
					'minv'	=> 8,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'new_password'	=> array(
					'name'	=> 'Nytt passord',
					'minv'	=> 8,
					'maxv'	=> 32,
					'must'	=> true
				),
				'rep_password'	=> array(
					'name'	=> 'Bekreft nytt passord',
					'same'	=> 'new_password',	// same as new_password
					'must'	=> true
				)
			));

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {

				if(Encrypt::make(IO::get('old_password'), $_init_uzer_data->SLS) == $_init_uzer_data->Password) {

					$sls = Encrypt::salt();
					$pas = Encrypt::make(IO::get('new_password'), $sls);

					$user = new User();
					$user->update(array(
						'SLS'		=> $sls,
						'Password'	=> $pas
					));

					/*
					-	Redirect the user.
					*/
						Session::flash('changepw', 'Alle endringer er lagret.');
						Redirect::to('/changepw');
				} else {
					$errors[] = "Passordet du har angitt er ikke korrekt.";
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }