<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline() || !$_init_uzer->hasPerm('sysadmin')) { Redirect::to("/"); }

/*
-	Get all users for listing
*/
	if(IO::exists('get')) {
		if(IO::get('action') == "users") {
			$users = new Get();
			$users->get_data('users', 'UserID', 'ASC');
			$users = $users->results();
		}

		if(IO::get('action') == "categories") {
			$categories = new Get();
			$categories->get_data('category', 'CatID', 'ASC');
			$categories = $categories->results();
		}

		if(IO::get('action') == "deleteuser" && is_numeric(IO::get('alt'))) {
			$del = new Del();
			$del->delete('users', IO::get('alt'), 'UserID');
			Session::flash('cpanel', 'Brukeren er slettet.');
			Redirect::to('/cpanel/users/');
		}

		if(IO::get('action') == "deletecat" && is_numeric(IO::get('alt'))) {
			$del = new Del();
			$del->delete('category', IO::get('alt'), 'CatID');
			Session::flash('cpanel', 'Kategorien er slettet.');
			Redirect::to('/cpanel/categories/');
		}

	}

/*
-	Adding new categories
*/
	    if(IO::exists()) {
	    	if(Token::check(IO::get('cat_token'))) {
	    		
	    		/*
				-	Validation with Validator method.
	    		*/

				$validate = new Validate();
				$validation = $validate->validator($_POST, array(
					'catname' => array(
						'name'	=> 'Kategori',	// if any errors display this name.
						'minv'	=> 1,			// the field required at least 6 characters.
						'maxv'	=> 128,			// the field can not hold more than 128 characters.
						'must'	=> true 		// the field can not be empty or blank.
					)
				));

				if ($validation->success()) {

					$handler = new Put();
					$handler->create('category', array("Catname" => strip_tags(IO::get('catname'))));
					Session::flash('cpanel', 'Kategorien er lagt til.');
					Redirect::to('/cpanel/categories/');

				} else {
					foreach($validation->errors() as $err) {
						$errors[] = $err;
					}
				}
	    	}
	    }