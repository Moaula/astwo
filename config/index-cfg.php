<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	gET ALL posix_times(oid) from database
*/	
	

	if(Cookie::exists("sort")) {
		$news_desc = md5("LATEST_FIRST");
		$news_rate = md5("TOP_RATED_FIRST");

		// get_data($table, $identifier = "ID", $order = "ASC", $limit = 1000)

		switch (Cookie::get('sort')) {
			case $news_desc:
				$posts = new Get();
				$posts->get_data('posts', 'PostID', 'DESC');
				$posts = $posts->results();
				break;

			case $news_rate:
				$posts = new Get();
				$posts->get_data('posts', 'Rating', 'DESC');
				$posts = $posts->results();
				break;
		}
	}

	if(!IO::exists('get') && !Cookie::exists("sort")) {
		$posts = new Get();
		$posts->get_data('posts', 'PostID');
		$posts = $posts->results();
	}

	if(IO::exists('get')) {
		if(is_numeric(IO::get('id'))) {
			$post = new Get();
			$post->get_data_by_id('posts', 'PostID', IO::get('id'));
			$post = $post->result();
		}

		if(!empty(IO::get('sort'))) {
			$sort = IO::get('sort');

			switch ($sort) {
				case 'ORGINAL':		
						Cookie::delete("sort");
						Redirect::to("/");
					break;

				case 'LATEST_FIRST':		
						Cookie::create("sort", md5("LATEST_FIRST"), 648000);
						Redirect::to("/");
					break;

				case 'TOP_RATED_FIRST':		
						Cookie::create("sort", md5("TOP_RATED_FIRST"), 648000);
						Redirect::to("/");
					break;
			}
		}
	}