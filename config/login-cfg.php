<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if($_init_uzer->isOnline()) { Redirect::to("/"); }

/*
-	Now we will go trough the posted input values and validate them with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('login_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'username' => array(
					'name'	=> 'Brukernavn',	// if any errors display this name.
					'minv'	=> 6,				// the field required at least 6 characters.
					'maxv'	=> 128,				// the field can not hold more than 128 characters.
					'must'	=> true 			// the field can not be empty or blank.
				),
				'password'	=> array(
					'name'	=> 'Passord',
					'minv'	=> 8,
					'maxv'	=> 32,
					'must'	=> true
				)
			));

			/*
			-	Now we will check if the user have
			-	selected to keepmein property so we can 
			-	let the user be logged inn for 365 days for the session.
			-	We will also check for password / username match in db.
			-	then we will log the user inn.
			*/

			
			if ($validation->success()) {

				$user = new User();
				$keepmein = (IO::get('keepmein') == "on") ? true : false;
				$logon = $user->login(IO::get('username'), IO::get('password'), $keepmein);
				if($logon) {
					Session::flash('index', 'Du er logget på.');
					Redirect::to("/");
				} else {
					$errors[] = "Brukernavn eller Passord stemmer ikke.";
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }