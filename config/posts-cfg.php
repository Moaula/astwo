<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }

/*
-	Get categories for the 
-	selection
*/
	if(IO::exists('get')) {
		if(IO::get('action') === "createnew" || IO::get('action') === "edit") {
			$categories = new Get();
			$categories->get_data('category', 'CatID');
			$categories = $categories->results();
		}
		
		if(IO::get('action') === "myposts") {
			$posts = new Get();
			if(!$_init_uzer->hasPerm('sysadmin')) {
				$posts->get_data_by_id('posts', 'UserID', $_init_uzer_data->UserID);
			} else {
				$posts->get_data('posts', 'UserID');
			}
			$posts = $posts->results();
		}

		if(IO::get('action') === "edit" && is_numeric(IO::get('id'))) {
			$post = new Get();
			$post->get_data_by_id('posts', 'PostID', IO::get('id'));
			$edit_post = $post->result();

			$post->get_data_by_id('category', 'CatID', $edit_post->category);
			$edit_cat = $post->result();

			if(!$_init_uzer->hasPerm('sysadmin')) {
				if($edit_post->UserID !== $_init_uzer_data->UserID) {
					Session::flash("index", "Du forsøkte å endre på noe som ikke tilhører deg!");
					Redirect::to("/?alert=danger");
				}
			}
		}

		if(IO::get('action') === "delete" && is_numeric(IO::get('id')) && !empty(IO::get('img'))) {
			
			if($_init_uzer->hasPerm('sysadmin')) {
				$del = new Del();
				$del->delete('posts', IO::get('id'), 'PostID');
				$file = "res/uploads/" . IO::get('img');
				$file_t = "res/uploads/t_" . IO::get('img');
				unlink($file); unlink($file_t);

				Session::flash('posts', 'Posten er slettet.');
				Redirect::to('/posts/myposts/');
			}

			$post = new Get();
			$post->get_data_by_id('posts', 'PostID', IO::get('id'));
			$edit_post = $post->result();

			if($edit_post->UserID !== $_init_uzer_data->UserID) {

				Session::flash("index", "Du forsøkte å endre på noe som ikke tilhører deg!");
				Redirect::to("/?alert=danger");
			
			} else {

				$del = new Del();
				$del->delete('posts', IO::get('id'), 'PostID');
				$file = "res/uploads/" . IO::get('img');
				$file_t = "res/uploads/t_" . IO::get('img');
				unlink($file); unlink($file_t);

				Session::flash('posts', 'Posten er slettet.');
				Redirect::to('/posts/myposts/');
			}
		}
	}
	


/*
-	Now we will go trough the posted 
-	input values and validate them
-	with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('add_new_post_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'post_title' => array(
					'name'	=> 'Tittel',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'post_content'	=> array(
					'name'	=> 'Innhold',
					'minv'	=> 1,
					'maxv'	=> 4096,
					'must'	=> true
				),
				'post_tags'	=> array(
					'name'	=> 'Tags',
					'minv'	=> 5,
					'maxv'	=> 255,
					'must'	=> true
				),
				'post_cat'	=> array(
					'name'	=> 'Kategori',
					'must'	=> true
				)
			));

			if(IO::exists("file")) {
				if(!empty($_FILES['post_image']['name'])) {
					$files = $_FILES['post_image'];
					$allow = array('jpg', 'png', 'jpeg');
					$dataf = "res/uploads/";

					if(!file_exists($dataf)) { 
						mkdir($dataf, 0755);
					}
					
					$file_temp = $files['tmp_name'];
					$file_size = $files['size'];
					$file_erro = $files['error'];
					$file_name = $files['name'];

					$ext = explode('.', $file_name);
					$ext = strtolower(end($ext));

					if (in_array($ext, $allow) === false) {
						$errors[] = 'Det er ingen support for fil typen du har valgt. Bare (JPG, JPEG, PNG) er tilatt.';
					}

					if (empty($errors) === true) {
						$genName = 'IN'.rand(1111111, 9999999).'NYC';
						$imgName = $genName.'.'.$ext;
						$imgName_t = "t_" . $imgName; 
						move_uploaded_file($file_temp, $dataf.$imgName);
						create_thumbs($dataf.$imgName, $dataf.$imgName_t, $ext, '720', '428');
					}
				}
			}

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success() && empty($errors)) {
				
				$handler = new Put();

				try {
					$handler->create('posts', array(
						'UserID'	=> $_init_uzer_data->UserID,
						'Title'		=> strip_tags(IO::get('post_title')),
						'Content'	=> strip_tags(IO::get('post_content')),
						'category'	=> IO::get('post_cat'),
						'image'		=> $imgName,
						'Tags'		=> strip_tags(IO::get('post_tags'))
					));	
				} catch (Exception $e) {
					$errors[] = $e->getMessage();
				}

				Session::flash('index', 'Din post er publisert.');
				Redirect::to('/');

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}


    	if(Token::check(IO::get('update_post_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'post_title' => array(
					'name'	=> 'Tittel',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'post_content'	=> array(
					'name'	=> 'Innhold',
					'minv'	=> 1,
					'maxv'	=> 4096,
					'must'	=> true
				),
				'post_tags'	=> array(
					'name'	=> 'Tags',
					'minv'	=> 5,
					'maxv'	=> 255,
					'must'	=> true
				),
				'post_cat'	=> array(
					'name'	=> 'Kategori',
					'must'	=> true
				)
			));

			if(IO::exists("file")) {
				if(!empty($_FILES['post_image']['name'])) {
					$files = $_FILES['post_image'];
					$allow = array('jpg', 'png', 'jpeg');
					$dataf = "res/uploads/";

					if(file_exists($dataf.IO::get('old_image'))) {
						unlink($dataf.IO::get('old_image'));
						unlink($dataf.'t_'.IO::get('old_image'));
					}

					if(!file_exists($dataf)) { 
						mkdir($dataf, 0755);
					}
					
					$file_temp = $files['tmp_name'];
					$file_size = $files['size'];
					$file_erro = $files['error'];
					$file_name = $files['name'];

					$ext = explode('.', $file_name);
					$ext = strtolower(end($ext));

					if (in_array($ext, $allow) === false) {
						$errors[] = 'Det er ingen support for fil typen du har valgt. Bare (JPG, JPEG, PNG) er tilatt.';
					}

					if (empty($errors) === true) {
						$genName = 'IN'.rand(1111111, 9999999).'NYC';
						$imgName = $genName.'.'.$ext;
						$imgName_t = "t_" . $imgName; 
						move_uploaded_file($file_temp, $dataf.$imgName);
						create_thumbs($dataf.$imgName, $dataf.$imgName_t, $ext, '720', '428');
					}
				} else {
					$imgName = IO::get('old_image');
				}
			} else {
				$imgName = IO::get('old_image');
			}

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success() && empty($errors)) {
				
				$handler = new Put();

				try {

					$handler->update('posts', array(
						'Title'		=> strip_tags(IO::get('post_title')),
						'Content'	=> strip_tags(IO::get('post_content')),
						'category'	=> IO::get('post_cat'),
						'image'		=> $imgName,
						'Tags'		=> strip_tags(IO::get('post_tags'))
					), IO::get('update_id'), 'PostID');	

				} catch (Exception $e) {
					$errors[] = $e->getMessage();
				}

				Session::flash('posts', 'Alle endringer er lagret.');
				Redirect::to('/posts/myposts/');

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }