<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if(!$_init_uzer->isOnline()) { Redirect::to("/"); }


/*
-	Now we will go trough the posted 
-	input values and validate them
-	with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('update_user_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				)
			));

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {

				if(Encrypt::make(IO::get('Password'), $_init_uzer_data->SLS) == $_init_uzer_data->Password) {

					$user = new User();
					$user->update(array(
						'FirstName'		=> strip_tags(strtolower(IO::get('firstname'))),
						'LastName'		=> strip_tags(strtolower(IO::get('lastname')))
					));

					/*
					-	Redirect the user.
					*/
						Session::flash('profile', 'Alle endringer er lagret.');
						Redirect::to('/profile');
				} else {
					$errors[] = "Passordet du har angitt er ikke korrekt.";
				}

			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }