<?php

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	We will check if the user is already signed inn
-	if the user is signed inn we will restrict them from
-	re-logging. Its forbidden.
*/
	if($_init_uzer->isOnline()) { Redirect::to("/"); }


/*
-	Now we will go trough the posted 
-	input values and validate them
-	with the validate method.
-	First we will check if there is any input posted, then we will check if the input is posted by the client or 
-	if some one is trying to post the data on behaf of someone else wish is not allowed. Therefor we have the token
-	to check for CSRF (cross site request forgery).
*/
	
    if(IO::exists()) {
    	if(Token::check(IO::get('reg_kunde_token'))) {
    		
    		/*
			-	Validation with Validator method.
    		*/

			$validate = new Validate();
			$validation = $validate->validator($_POST, array(
				'firstname' => array(
					'name'	=> 'Fornavn',	// if any errors display this name.
					'minv'	=> 2,			// the field required at least 2 characters.
					'maxv'	=> 32,			// the field can not hold more than 32 characters.
					'must'	=> true 		// the field can not be empty or blank.
				),
				'lastname'	=> array(
					'name'	=> 'Etternavn',
					'minv'	=> 2,
					'maxv'	=> 32,
					'must'	=> true
				),
				'Username'	=> array(
					'name'	=> 'Brukernavn',
					'minv'	=> 6,
					'maxv'	=> 12,
					'prim'	=> 'users',
					'must'	=> true
				),
				'password'	=> array(
					'name'	=> 'Passord',
					'minv'	=> 8,
					'maxv'	=> 32,
					'must'	=> true
				),
				'passwordr'	=> array(
					'name'	=> 'Bekreft passord',
					'same'	=> 'password',
					'must'	=> true
				)

			));

			/*
			-	if the validation process is successfull
			-	then we can continue to addint the information 
			-	to the database. 
			-	But however first we check if the user have agreed to 
			-	Terms Of Service
			*/

			
			if ($validation->success()) {
				if(IO::get('termsofservice') == 'on') {

					/*
					-	Making the passwords and 
					-	encryption required.
					-	-------------------------
					-	Connecting to the database
					-	and getteing ready for insertion.
					*/
						$sls = Encrypt::salt();
						$pass = Encrypt::make(IO::get('password'), $sls);
						$user = new User();

					/*
					-	commit - db changes.
					*/
						try {
							$user->create(array(
								'FirstName'		=> strip_tags(strtolower(IO::get('firstname'))),
								'LastName'		=> strip_tags(strtolower(IO::get('lastname'))),
								'Username'		=> strip_tags(strtolower(IO::get('username'))),
								'SLS'			=> $sls,
								'Password'		=> $pass,
								'UserGroup'		=> 1
							));

							/*
							-	Redirect the user.
							*/
								Session::flash('signup', 'Du har suksessfult blit medlem, du kan loggepå nå. ('. IO::get('username') .')');
								Redirect::to('/signup/kunde/success/');

						} catch (Exception $e) {
							$errors[] = $e->getMessage();
						}

				} else {
					$errors[] = "Vennligst godta <strong>Vilkår</strong> og <strong>Personverns lover</strong> for å fortsette.";
				}
			} else {
				foreach($validation->errors() as $err) {
					$errors[] = $err;
				}
			}
    	}
    }