<?php 
	include_once('config/cpanel-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container large-form form-center">
			<?php 
				// feedback 
				if(Session::exists('cpanel')) { 
					echo writealerts(Session::flash('cpanel'), 'success', 'flat'); 
				} 

				// error feedback
				if(!empty($errors)) { 
					echo writeerrors($errors, 'flat', 'Feil'); 
				} 
			?>

			<!--======================================
			#	Listing users for administration
			#	Deleteing users  
			=========================================-->

			<?php if(IO::exists('get') && IO::get('action') === "users") { ?>
				<div class="entry-title">Registred users</div>
				<?php if(!empty($users)) { ?>
					<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th>#</th>
								<th><i class="fa fa-user"></i> &nbsp; Brukernavn </th>
								<th><i class="fa fa-leaf"></i> &nbsp; Fornavn</th>
								<th><i class="fa fa-leaf"></i> &nbsp; Etternavn</th>
								<th><i class="fa fa-cogs"></i> &nbsp; Administrate</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach ($users as $user) { ?>
								<?php if($user->UserID !== $_init_uzer_data->UserID) { ?>
											
									<tr>
										<td><?php echo $user->UserID; ?></td>
										<td><?php echo ucfirst($user->Username); ?></td>
										<td><?php echo ucfirst($user->Firstname); ?></td>
										<td><?php echo ucfirst($user->Lastname); ?></td>
										<td>
											<a onclick="return confirm('Er du sikker du vil slette <?php echo ucfirst($user->Username); ?>');" href="cpanel/deleteuser/<?php echo $user->UserID; ?>/" class="btn btn-sm btn-danger btn-block"><i class="fa fa-trash"></i> &nbsp; Slett </a>
										</td>
									</tr>

								<?php } ?> 
							<?php } ?>
						</tbody>
					</table> 
				<?php } ?>
			<?php } ?>			

			<!--======================================
			#	Listing categories for administration
			#	Deleteing categories  
			=========================================-->

			<?php if(IO::exists('get') && IO::get('action') === "categories") { ?>
				<div class="entry-title">Registred categories</div>

				<form action="" method="post">
					<div class="form-group">
						<label>Kategorinavn</label>
						<input type="text" name="catname" placeholder="Skriv inn kategori" class="form-control">
					</div>
					<div class="form-group">
						<input type="hidden" name="cat_token" value="<?php echo Token::create(); ?>" />
						<button class="btn btn-primary"><i class="fa fa-plus"></i> &nbsp; Legg til</button>
					</div>
				</form>

				<hr/>

				<?php if(!empty($categories)) { ?>
					<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th>#</th>
								<th><i class="fa fa-user"></i> &nbsp; Kategori </th>
								<th><i class="fa fa-cubes"></i> &nbsp; Used in news </th>
								<th><i class="fa fa-cogs"></i> &nbsp; Administrate</th>
							</tr>
						</thead>

						<tbody>
							<?php foreach ($categories as $category) { ?>
							<?php  
									$count = new Get();
									$count->vote_check('posts', array("category" => $category->CatID), "PostID");
									$count = $count->count();
							?>		
								<tr>
									<td><?php echo $category->CatID; ?></td>
									<td><?php echo ucfirst($category->Catname); ?></td>
									<td><?php if($count) { echo $count; } else { echo "0"; } ?></td>
									<td>
										<a onclick="return confirm('Er du sikker du vil slette <?php echo ucfirst($category->Catname); ?>');" href="cpanel/deletecat/<?php echo $category->CatID; ?>/" class="btn btn-sm btn-danger btn-block"><i class="fa fa-trash"></i> &nbsp; Slett </a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table> 
				<?php } ?>
			<?php } ?>


		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>