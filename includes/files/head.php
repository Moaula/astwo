<!DOCTYPE html>
<html lang="nb"><head>
      <!-- Base -->
      <base href="http://swax.no/">

      <!-- Website title -->
      <title> Infinite News </title>

      <!-- Favorite Icon -->
      <link rel="icon" type="image/x-icon" href="res/images/logo-small.png">

      <!-- Meta -->
      <meta charset="utf-8">
      <meta name="description" content="Infinite news from around the world. Based on real and fake events.">
      <meta name="keywords" content="infinite, news, from, around, the, world, based, on, real, and, fake, events">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- css -->
      <link rel="stylesheet" type="text/css" href="appfiles/css/plugins/bootstrap.css">
      <link rel="stylesheet" type="text/css" href="appfiles/css/plugins/font-awesome.css">
      <link rel="stylesheet" type="text/css" href="appfiles/css/main.css">

      <!-- Script -->
      <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
      <script type="text/javascript" src="appfiles/js/plugins/bootstrap.min.js"></script>
      <script type="text/javascript" src="appfiles/js/functions.js"></script>
      <script type="text/javascript" src="appfiles/js/main.js"></script>
</head><body>