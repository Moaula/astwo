
<nav class="navbar navbar-default  navbar-fixed-top">
<div class="container">
    <div class="container-fluid">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Meny</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img alt="Brand" src="res/images/logo-small.png" class="brand-logo">
                <div class="brand-logo-text">Infinite News</div>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="/" <?php if($CDoc == "index") { echo 'class="red-circle"'; } ?> ><i class="fa fa-home"></i> &nbsp; Home</a></li>
                <?php if(!$_init_uzer->isOnline()) { ?>
                    <li><a <?php if($CDoc == "signup") { echo 'class="red-circle"'; } ?> href="/signup"><i class="fa fa-user-plus"></i> &nbsp; Registrer</a></li>
                    <li><a <?php if($CDoc == "login") { echo 'class="red-circle"'; } ?> href="/login"><i class="fa fa-sign-in"></i> &nbsp; Logg på</a></li>
                <?php } ?>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <!-- Filtering menus based on logged in or not -->
                <?php if($_init_uzer->isOnline()) { ?>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user"></i> &nbsp; Min konto <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/profile"><i class="fa fa-user"></i> &nbsp; Min profil</a></li>
                            <li><a href="/changepw"><i class="fa fa-refresh"></i> &nbsp; Bytt passord</a></li>
                            <li><a href="/posts/createnew/"><i class="fa fa-plus"></i> &nbsp; Legg til nyhet </a></li>
                            <li><a href="/posts/myposts/"><i class="fa fa-eye"></i> &nbsp; Mine nyheter </a></li>
                            <?php if($_init_uzer->hasPerm('admin')) { ?>
                                <li role="separator" class="divider"></li>
                                <li role="separator" class="dropdown-header"><i class="fa fa-arrow-down"></i>  &nbsp; Control options &nbsp; <i class="fa fa-arrow-down"></i></li>
                                <li><a href="cpanel/users/"><i class="fa fa-list"></i> &nbsp; List users </a></li>
                                <li><a href="cpanel/categories/"><i class="fa fa-list"></i> &nbsp; List categories </a></li>
                                
                                <li role="separator" class="dropdown-header"><i class="fa fa-arrow-up"></i>  &nbsp; Control options &nbsp; <i class="fa fa-arrow-up"></i></li>
                            <?php } ?>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout"><i class="fa fa-power-off"></i> &nbsp; Logg ut</a></li>
                        </ul>
                    </li>
                   
                <?php } ?>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</div>
</nav>  