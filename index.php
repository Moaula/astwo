<?php 
	include_once('config/index-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<?php 
			// feedback 
			if(Session::exists('index')) {
				$alert = "success"; 
				if(!empty(IO::get('alert'))) { $alert = IO::get('alert'); }
				echo writealerts(Session::flash('index'), $alert, 'flat'); 
			} 
		?>

			<div class="sort-bar">
				Sorter etter: &nbsp; <i class="fa fa-sort"></i> &nbsp;
				<a href="/?sort=ORGINAL" class="btn btn-sm btn-default"><i class="fa fa-cube"></i> &nbsp; Standard</a>
				<a href="/?sort=LATEST_FIRST" class="btn btn-sm btn-default"><i class="fa fa-bars"></i> &nbsp; Siste nytt</a>
				<a href="/?sort=TOP_RATED_FIRST" class="btn btn-sm btn-default"><i class="fa fa-cubes"></i> &nbsp; Mest stjerner</a>
			</div>

		<?php
			if(!IO::exists('get')) {
				if(!empty($posts)) { 
					echo '<div class="row">';
					foreach ($posts as $post) {
		?>				
						<div class="col-md-6">
							<div class="post-container large-form form-center">
								<div class="entry-title"><?php echo ucfirst($post->Title); ?></div>

								<div class="post-image">
									<img src="res/uploads/t_<?php echo $post->image; ?>" alt="Post Image Missing">
								</div>
								<div class="post-content">
									<?php echo nl2br(substr($post->Content, 0, 500)) ?>...... <br /><br />
									<a href="/view/<?php echo $post->PostID; ?>/" class="btn btn-default">Les mer...</a>
									<?php 
										if($_init_uzer->isOnline()) {
											if($post->UserID == $_init_uzer_data->UserID || $_init_uzer->hasPerm('sysadmin')) { 
									?>
												<a href="posts/edit/<?php echo $post->PostID; ?>/" class="btn btn-default"><i class="fa fa-pencil"></i> &nbsp; Endre</a>
												<a onclick="return confirm('Er du sikker du ønsker å slette denne artikkelen ?\nDet er ingen vei tilabke, og (<?php echo $post->Title; ?>) blir permananet slettet med alle dataene, bilder.')" href="posts/delete/<?php echo $post->PostID; ?>/<?php echo $post->image; ?>/" class="btn btn-default"><i class="fa fa-trash"></i> &nbsp; Slett</a>
									<?php 
											}
										} 
									?>
								</div>
								<div class="post-controls">
									<a href="#" class="btn btn-default" disabled ><i class="fa fa-star"></i> &nbsp; Total <span class="votes_<?php echo $post->PostID; ?>"><?php echo $post->Rating; ?></span> </a>
									<?php if($_init_uzer->isOnline()) { ?>
										<button data-do="take" data-id="<?php echo $post->PostID; ?>" data-postfix="<?php echo $post->Rating; ?>" class="btn btn-default initvote" ><i class="fa fa-star-o"></i> &nbsp; Ta 1 stjerne </button>
										<button data-do="give" data-id="<?php echo $post->PostID; ?>" data-postfix="<?php echo $post->Rating; ?>" class="btn btn-default initvote" ><i class="fa fa-star"></i> &nbsp; Gi 1 stjerne </button>
									<?php } else { ?>
										<a href="/signup" onclick="return confirm('Du må være medlem for å stemme opp eller ned')"  class="btn btn-default"><i class="fa fa-star-o"></i> &nbsp; Ta 1 stjerne</a>
										<a href="/signup" onclick="return confirm('Du må være medlem for å stemme opp eller ned')"  class="btn btn-default"><i class="fa fa-star"></i> &nbsp; Gi 1 stjerne </a>
									<?php } ?>
									<a href="#" class="btn btn-default control-left" disabled ><i class="fa fa-eye"></i> &nbsp; Views <?php echo $post->Views; ?></a>
								</div>
							</div>
						</div>
						
		<?php 
					}
					echo "</div>";
				}
			} else {
				if(!empty($post)) {
					$handle = new Put();
					$handle->update("posts", array("Views" => ($post->Views + 1)), $post->PostID, "PostID");
		?>

					<div class="post-container large-form form-center">
						<div class="entry-title"><?php echo ucfirst($post->Title); ?></div>

						<div class="post-image">
							<img src="res/uploads/t_<?php echo $post->image; ?>" alt="Post Image Missing">
						</div>
						<div class="post-content">
							<?php echo nl2br($post->Content) ?>
						</div>
						<div class="post-controls">
							<a href="#" class="btn btn-default" disabled ><i class="fa fa-star"></i> &nbsp; Total <span class="votes_<?php echo $post->PostID; ?>"><?php echo $post->Rating; ?></span> </a>
							<?php if($_init_uzer->isOnline()) { ?>
								<button data-do="take" data-id="<?php echo $post->PostID; ?>" data-postfix="<?php echo $post->Rating; ?>" class="btn btn-default initvote" ><i class="fa fa-star-o"></i> &nbsp; Ta 1 stjerne </button>
								<button data-do="give" data-id="<?php echo $post->PostID; ?>" data-postfix="<?php echo $post->Rating; ?>" class="btn btn-default initvote" ><i class="fa fa-star"></i> &nbsp; Gi 1 stjerne </button>
							<?php } else { ?>
								<a href="/signup" onclick="return confirm('Du må være medlem for å stemme opp eller ned')"  class="btn btn-default"><i class="fa fa-star-o"></i> &nbsp; Ta 1 stjerne</a>
								<a href="/signup" onclick="return confirm('Du må være medlem for å stemme opp eller ned')"  class="btn btn-default"><i class="fa fa-star"></i> &nbsp; Gi 1 stjerne </a>
							<?php } ?>
							<a href="#" class="btn btn-default control-left" disabled ><i class="fa fa-eye"></i> &nbsp; Views <?php echo $post->Views; ?></a>
						</div>
					</div>

		<?php 
				}
			} 
		?>
	</div>




<?php 
	include_once('includes/files/botarea.php'); 
?>