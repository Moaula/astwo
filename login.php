<?php 
	include_once('config/login-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>
	<!-- Container for options panel -->
	<div class="container">
		<div class="inner-container small-form form-center">
			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors, '', 'En feil har oppstått.'); 
				} 
			?>

			<!-- Flashing sessions with infromation -->
			<?php 
				if(Session::exists('login')) { 
					echo writealerts(Session::flash('login'), 'success'); 
				} 
			?>

			<div class="entry-title">Logg på</div>
			<form action="" method="post" enctype="multipart/form-data" class="form-body" role="signup">
				<div class="form-group">
					<label>Brukernavn</label>
					<input type="text" name="username" class="form-control" value="<?php echo IO::get('username'); ?>" placeholder="Username" />
				</div>

				<div class="form-group">
					<label>Passord</label>
					<input type="password" name="password" class="form-control" placeholder="Skriv inn ditt passord" />
				</div>

				<div class="form-group">
					<label><input name="keepmein" type="checkbox" /> Forbli pålogget.</label>
				</div>

				<div class="form-group">
					<input type="hidden" name="login_token" value="<?php echo Token::create(); ?>">
					<button class="btn btn-primary">Logg på &nbsp; <i class="fa fa-sign-in"></i> </button>
				</div>
			</form>
		</div>
	</div>




<?php 
	include_once('includes/files/botarea.php'); 
?>