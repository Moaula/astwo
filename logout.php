<?php

//	Include files:
	include_once('appfiles/core/init.php');

//	Check if the user is logged in from before
//	then redirect the user to the homepage.
if (!$_init_uzer->isOnline()) { Redirect::to('/?ref=NotLoggedInn'); }
if ($_init_uzer->isOnline()) { $_init_uzer->logout(); Redirect::to('/'); } 