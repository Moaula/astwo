<?php 
	include_once('config/posts-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container large-form form-center">
			<?php 
				// feedback 
				if(Session::exists('posts')) { 
					echo writealerts(Session::flash('posts'), 'success', 'flat'); 
				} 

				// error feedback
				if(!empty($errors)) { 
					echo writeerrors($errors, 'flat', 'Feil'); 
				} 
			?>


			<?php if(IO::exists('get') && IO::get('action') === "createnew") { ?>

			<!-- Legge til nyheter -->
			<div class="entry-title">Legg til nyhet</div>
			<div class="row">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Overskrift</label>
							<input type="text" name="post_title" class="form-control" placeholder="Hva skal du skriv om ?" />
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Innhold</label>
							<textarea name="post_content" class="form-control" rows="6"></textarea>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Bilde</label>
							<input type="file" name="post_image" />
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Kategori</label>
							<select name="post_cat" class="form-control">
								<option selected="selected">Velg Kategori</option>
								<?php foreach ($categories as $cat) { ?>
									<option value="<?php echo $cat->CatID; ?>"><?php echo $cat->Catname; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Tegs</label>
							<input type="text" name="post_tags" class="form-control" placeholder="Tagg posten så den kan søkes opp, seperer med komma." />
						</div>
					</div>


					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<input type="hidden" name="add_new_post_token" value="<?php echo Token::create(); ?>">
							<button class="btn btn-primary"><i class="fa fa-globe"></i> &nbsp; Publiser </button>
							<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>
			</div>
			<!-- Legge til nyheter END -->

			<?php } ?>

			<?php if(IO::exists('get') && IO::get('action') === "myposts") { ?>

			<!-- Legge til nyheter -->
			<div class="entry-title">Mine nyheter <?php if($_init_uzer->hasPerm('sysadmin')) { echo '<i class="fa fa-star"></i> YOU ARE AN SYSTEM ADMIN '; } ?></div>
			<div class="row">

				<?php 
					if(!empty($posts)) {
						foreach($posts as $post) { 
				?>

						<div class="col-md-12 col-sm-12 col-lg-12">
							<div class="post-bar">
								<div class="post-bar-left">
									<img src="res/uploads/t_<?php echo $post->image; ?>">
								</div>
								<div class="post-bar-middle">
									<div class="post-bar-middle-title"><strong><?php echo $post->Title; ?></strong></div>
									<p>
										<?php echo substr($post->Content, 0, 100); ?> [....]
									</p>
								</div>
								<div class="post-bar-right">
									<a href="view/<?php echo $post->PostID; ?>/" class="btn btn-default btn-block"><i class="fa fa-eye"></i> &nbsp; Lese</a>
									<a href="posts/edit/<?php echo $post->PostID; ?>/" class="btn btn-default btn-block"><i class="fa fa-pencil"></i> &nbsp; Endre</a>
									<a onclick="return confirm('Er du sikker du ønsker å slette denne artikkelen ?\nDet er ingen vei tilabke, og (<?php echo $post->Title; ?>) blir permananet slettet med alle dataene, bilder.')" href="posts/delete/<?php echo $post->PostID; ?>/<?php echo $post->image; ?>/" class="btn btn-default btn-block"><i class="fa fa-trash"></i> &nbsp; Slett</a>
								</div>
							</div>
						</div>

				<?php
						} 
					} else {
						echo '<div class="col-md-12 col-sm-12 col-lg-12">';
						echo "Du har ikke skrevet noen nyheter enda.";
						echo '</div>';
					} 
				?>
			</div>
			<!-- Legge til nyheter END -->

			<?php } ?>


			<?php if(IO::exists('get') && IO::get('action') === "edit" && is_numeric(IO::get('id'))) { ?>

			<!-- Endring av artkiler -->
			<div class="entry-title">Endring av | <?php echo $edit_post->Title; ?></div>
			<div class="row">
				<form action="" method="post" enctype="multipart/form-data">
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Overskrift</label>
							<input type="text" name="post_title" value="<?php echo $edit_post->Title; ?>" class="form-control" placeholder="Hva skal du skriv om ?" />
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Innhold</label>
							<textarea name="post_content" class="form-control" rows="6"><?php echo $edit_post->Content; ?></textarea>
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group current_image">
							<label>Bilde</label>
							<input type="file" name="post_image" /> <br />
							<label>Gjeldene Bilde</label><br />
							<img src="res/uploads/t_<?php echo $edit_post->image; ?>" alt="current image">
							<input type="hidden" name="old_image" value="<?php echo $edit_post->image; ?>">
						</div>
					</div>

					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Kategori</label>
							<select name="post_cat" class="form-control">
								<option selected="selected" value="<?php echo $edit_post->category; ?>">Valgt ( <?php echo $edit_cat->Catname; ?> )</option>
								<?php foreach ($categories as $cat) { ?>
									<option value="<?php echo $cat->CatID; ?>"><?php echo $cat->Catname; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Tegs</label>
							<input type="text" name="post_tags" value="<?php echo $edit_post->Tags; ?>" class="form-control" placeholder="Tagg posten så den kan søkes opp, seperer med komma." />
						</div>
					</div>


					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<input type="hidden" name="update_post_token" value="<?php echo Token::create(); ?>">
							<input type="hidden" name="update_id" value="<?php echo $edit_post->PostID; ?>">
							<button class="btn btn-primary"><i class="fa fa-globe"></i> &nbsp; Publiser </button>
							<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
						</div>
					</div>
				</form>
			</div>
			<!-- SLUTTER END -->

			<?php } ?>

		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>