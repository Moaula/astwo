<?php 
	include_once('config/profile-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>

	<div class="container">
		<div class="inner-container medium-form form-center">
			<?php 
				// feedback 
				if(Session::exists('profile')) { 
					echo writealerts(Session::flash('profile'), 'success', 'flat'); 
				} 

				// error feedback
				if(!empty($errors)) { 
					echo writeerrors($errors, 'flat', 'Feil'); 
				} 
			?>

			<div class="entry-title">Min info</div>
			<div class="row">
				<form action="" method="post">
					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Fornavn</label>
							<input type="text" name="firstname" class="form-control" value="<?php echo ucfirst($_init_uzer_data->Firstname); ?>" />
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-lg-6">
						<div class="form-group">
							<label>Etternavn</label>
							<input type="text" name="lastname" class="form-control" value="<?php echo ucfirst($_init_uzer_data->Lastname); ?>" />
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Brukernavn</label>
							<input type="text" name="dummy" disabled class="form-control" value="<?php echo ucfirst($_init_uzer_data->Username); ?>" />
						</div>
					</div>

					<div class="col-md-12 col-sm-12 col-lg-12">
						<div class="form-group">
							<label>Skriv inn passord for å oppdatere</label>
							<input type="password" name="Password" class="form-control" placeholder="Skriv inn passordet ditt for å oppdatere info..." />
						</div>
					</div>


					<div class="col-md-12 col-sm-12 col-lg-12">
						<input type="hidden" name="update_user_token" value="<?php echo Token::create(); ?>">
						<button class="btn btn-primary"><i class="fa fa-refresh"></i> &nbsp; Oppdater </button>
						<a href="/" class="btn btn-default"><i class="fa fa-remove"></i> &nbsp; Avbryt </a>
					</div>
				</form>
			</div>
		</div>
	</div>



<?php 
	include_once('includes/files/botarea.php'); 
?>