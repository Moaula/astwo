<?php 

//	Include files:
	include_once('appfiles/core/init.php');

/*
-	Update votes
*/

	if(IO::exists()) {
		$do = IO::get('vdo'); $id = IO::get('id');
		$id = (int)$id; $votes = IO::get('votes');
		$votes = ($votes + 1);
		$userid = $_init_uzer_data->UserID;

		$check = new Get();
		if(!$check->vote_check("votes", array("UserID" => $userid, "PostID" => $id), "VoteID")) {
			try {

				$handler = new Put();
				$handler->update("posts", array(
					"Rating" => $votes
				), $id, "PostID");

				$handler->create("votes", array(
					"UserID" => $userid,
					"PostID" => $id
				));

				echo $votes;

			} catch (Exception $e) {
				echo "Du har allerede stemt.";
			}
		} else {
			echo "Du har allerede stemt.";
		}

	}