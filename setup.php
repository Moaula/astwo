<?php 

	$db_conx = mysqli_connect("127.0.0.1", "root", "", "test"); 

	$sql =	"
			CREATE TABLE `users` (
			  `UserID` INTEGER(11) NOT NULL AUTO_INCREMENT,
			  `Username` VARCHAR(12) NOT NULL,
			  `Password` VARCHAR(64) NOT NULL,
			  `SLS` VARCHAR(32) NOT NULL,
			  `Firstname` VARCHAR(32) NOT NULL,
			  `Lastname` VARCHAR(32) NOT NULL,
			  `Usergroup` INTEGER(11) NOT NULL DEFAULT  1,
			  PRIMARY KEY (`UserID`)
			);";

	$query = mysqli_query($db_conx, $sql);


	$sql =	"
			CREATE TABLE `usergroups` (
			  `UserGroupID` INTEGER(11) NOT NULL AUTO_INCREMENT,
			  `Permissions` VARCHAR(255) NOT NULL,
			  `Name` VARCHAR(18) NOT NULL,
			  `UserDesc` VARCHAR(32) NOT NULL,
			  PRIMARY KEY (`UserGroupID`)
			);";

	$query = mysqli_query($db_conx, $sql);

	$sql =	"
			CREATE TABLE `sessionscookies` (
			  `SessionID` INTEGER(11) NOT NULL AUTO_INCREMENT,
			  `UserID` INTEGER(11) NOT NULL,
			  `SessionName` VARCHAR(64) NOT NULL,
			  PRIMARY KEY (`SessionID`)
			);";

	$query = mysqli_query($db_conx, $sql);	


	$sql = 	"
			CREATE TABLE `posts` (
			  `PostID` INTEGER NOT NULL AUTO_INCREMENT,
			  `UserID` INTEGER(11) NOT NULL,
			  `Title` VARCHAR(64) NOT NULL,
			  `Content` TEXT NOT NULL,
			  `category` INTEGER NULL DEFAULT NULL,
			  `image` VARCHAR(64) NOT NULL,
			  `Tags` VARCHAR(255) NOT NULL,
			  `Views` INTEGER NOT NULL DEFAULT 0,
			  `Rating` INTEGER NOT NULL DEFAULT 0,
			  PRIMARY KEY (`PostID`)
			);";

	$query = mysqli_query($db_conx, $sql);


	$sql = 	"
			CREATE TABLE `category` (
			  `CatID` INTEGER(11) NOT NULL AUTO_INCREMENT,
			  `Catname` VARCHAR(32) NOT NULL,
			  PRIMARY KEY (`CatID`)
			);";

	$query = mysqli_query($db_conx, $sql);


	$sql =	"
			CREATE TABLE `votes` (
			  `VoteID` INTEGER(11) NOT NULL AUTO_INCREMENT,
			  `UserID` INTEGER(11) NOT NULL,
			  `PostID` INTEGER(11) NOT NULL,
			  PRIMARY KEY (`VoteID`)
			);";
    $query = mysqli_query($db_conx, $sql);

    $sql =	"ALTER TABLE `users` ADD FOREIGN KEY (Usergroup) REFERENCES `usergroups` (`UserGroupID`);".
			"ALTER TABLE `sessionscookies` ADD FOREIGN KEY (UserID) REFERENCES `users` (`UserID`);".
			"ALTER TABLE `posts` ADD FOREIGN KEY (UserID) REFERENCES `users` (`UserID`);".
			"ALTER TABLE `posts` ADD FOREIGN KEY (category) REFERENCES `category` (`CatID`);".
			"ALTER TABLE `votes` ADD FOREIGN KEY (UserID) REFERENCES `users` (`UserID`);".
			"ALTER TABLE `votes` ADD FOREIGN KEY (PostID) REFERENCES `posts` (`PostID`);";

    $query = mysqli_query($db_conx, $sql);


    $sql =	"
    		INSERT INTO `usergroups` (`UserGroupID`, `Permissions`, `Name`, `UserDesc`) VALUES
			(1, '{\"operator\":0, \"moderator\":0, \"admin\":0, \"sysadmin\":0}', 'U_STD', 'USER'),
			(2, '{\"operator\":1, \"moderator\":0, \"admin\":0, \"sysadmin\":0}', 'U_OPERATOR', 'OPERATØR'),
			(3, '{\"operator\":1, \"moderator\":1, \"admin\":0, \"sysadmin\":0}', 'U_MOD', 'MODERATOR'),
			(4, '{\"operator\":1, \"moderator\":1, \"admin\":1, \"sysadmin\":0}', 'U_ADMIN', 'ADMINISTRATOR'),
			(6, '{\"operator\":1, \"moderator\":1, \"admin\":1, \"sysadmin\":1}', 'U_SYSADMIN', 'SYSTEM ADMINISTRATOR');";
	
	$query = mysqli_query($db_conx, $sql);


    echo "done";

?>