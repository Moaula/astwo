<?php 
	include_once('config/signup-cfg.php'); 
	include_once('includes/files/toparea.php'); 
?>
	<!-- Container for options panel -->
	<div class="container">
		
		<!-- Kunde registrering container -->
		<div class="registration-from medium-form form-center">
			<!-- Error outputting -->
			<?php 
				if(!empty($errors)) { 
					echo writeerrors($errors); 
				} 
			?>


			<?php if(IO::exists('get') && IO::get('action') == 'kunde' && IO::get('alt') == 'success') { ?>
				<div class="form-title">Gratulerer</div>

				<!-- Flashing sessions with infromation -->
				<?php 
					if(Session::exists('signup')) { 
						echo writealerts(Session::flash('signup'), 'success'); 
					} 
				?>

				<a href="/" class="btn btn-primary btn-lg btn-block"><i class="fa fa-home"></i> &nbsp; Tilbake til hjemmesiden </a>

			<?php } else { ?>

			
			
				<div class="form-title">registerer deg</div>
				<form action="" method="post" enctype="multipart/form-data" class="form-body" role="signup">
					
						

					<div class="form-group">
						<label>Fornavn</label>
						<input type="text" name="firstname" placeholder="Hva er ditt navn?" class="form-control" value="<?php echo IO::get('firstname'); ?>" />
					</div>

					<div class="form-group">
						<label>Etternavn</label>
						<input type="text" name="lastname" placeholder="Hva er ditt etternavn?" class="form-control" value="<?php echo IO::get('lastname'); ?>" />
					</div>

					<div class="form-group">
						<label>Brukernavn</label>
						<input type="text" name="username" placeholder="f.eks: Blackgolf" class="form-control" value="<?php echo IO::get('username'); ?>" />
					</div>


					<div class="form-group">
						<label>Ønsket passord (minst 8 tegn)</label>
						<input type="password" name="password" placeholder="Skriv inn ønsket passord" class="form-control" />
					</div>

					<div class="form-group">
						<label>Bekreft passord</label>
						<input type="password" name="passwordr" placeholder="Gjenta ønsket passord" class="form-control" />
					</div>

					<div class="form-group bottom-group">
						<label><input name="termsofservice" type="checkbox" /> Jeg har lest og godtatt <a href="/tos">Vilkår</a>.</label>
						<input type="hidden" name="reg_kunde_token" value="<?php echo Token::create(); ?>">
						<button class="btn btn-primary btn-lg btn-block"><i class="fa fa-rocket"></i> BLI MEDLEM</button>
					</div>

				</form><!-- end kunde registering div -->
			<?php } ?>
		</div>


	</div>




<?php 
	include_once('includes/files/botarea.php'); 
?>